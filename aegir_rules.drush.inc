<?php

/**
 * @file
 * Aegir Rules module drush integration.
 */

/**
 * Implements drush_hook_pre_hosting_task().
 */
function drush_aegir_rules_pre_hosting_task() {
  $task = &drush_get_context('HOSTING_TASK');

  include_once 'aegir_rules.rules.inc';
  $definitions = aegir_rules_event_definitions();

  if (isset($definitions[$task->ref->type]['tasks'][$task->task_type])) {
    rules_invoke_event('pre_hosting_' . $task->ref->type . '_' . $task->task_type, $task, $task->ref);
  }

  rules_invoke_event('pre_hosting_default_execute', $task, $task->ref);

}
