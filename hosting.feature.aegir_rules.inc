<?php
/**
 * @file
 * Expose the aegir rules feature to hostmaster.
 */

/**
 * Implements hook_hosting_feature().
 */
function hosting_aegir_rules_hosting_feature() {
  $features['aegir_rules'] = array(
    'title' => t('Aegir Rules'),
    'description' => t('Rules integration for Aegir.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'aegir_rules',
    'group' => 'experimental',
  );
  return $features;
}
