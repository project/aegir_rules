Aegir Rules
http://drupal.org/project/aegir_rules
====================================

DESCRIPTION
-----------
Aegir Rules provides Rules integration for the Aegir hosting system

FEATURES
-----------
- Rules events for all default AeGir hosting tasks
- Rules conditions for Aegir hosting tasks status values

REQUIREMENTS
-----------
- An active Aegir installation
- Ability to install modules to the Aegir Hostmaster site.
- Rules module

INSTALLATION
-----------
1) Download Rules and Aegir Rules modules
2) Move both modules to the Aegir hostmaster site module directory.
3) Enable both modules from Aegir module admin page
4) You probably also want to enable the rules_admin module, bundled with rules.



EXAMPLES
--------

These can be imported on the /admin/config/workflow/rules/reaction/import page.

{ "rules_mail_about_failed_task" : {
    "LABEL" : "Mail admin about failed task",
    "PLUGIN" : "reaction rule",
    "OWNER" : "rules",
    "TAGS" : [ "hosting task" ],
    "REQUIRES" : [ "aegir_rules", "rules" ],
    "ON" : { "post_hosting_default_execute" : [] },
    "IF" : [
      { "aegir_rules_condition_task_status_failure" : { "task" : [ "task" ] } }
    ],
    "DO" : [
      { "mail" : {
          "to" : "[site:mail]",
          "subject" : "Task failed: [task:title]",
          "message" : "I\u0027m sorry to report that a task failed.\r\n\r\n[task:title]\r\n\r\n\r\nVisit this url for the task logs:\r\n[task:url]",
          "language" : [ "" ]
        }
      }
    ]
  }
}



{ "rules_registry_rebuild" : {
    "LABEL" : "Registry rebuild",
    "PLUGIN" : "rule",
    "OWNER" : "rules",
    "TAGS" : [ "hosting task" ],
    "REQUIRES" : [ "rules" ],
    "USES VARIABLES" : { "trigger_task" : { "label" : "Triggering task", "type" : "node" } },
    "IF" : [
      { "entity_is_of_type" : { "entity" : [ "trigger-task" ], "type" : "node" } },
      { "entity_is_of_bundle" : {
          "entity" : [ "trigger-task" ],
          "type" : "node",
          "bundle" : { "value" : { "task" : "task" } }
        }
      }
    ],
    "DO" : [
      { "entity_create" : {
          "USING" : {
            "type" : "node",
            "param_type" : "task",
            "param_title" : "[trigger-task:title]",
            "param_author" : [ "trigger-task:author" ]
          },
          "PROVIDE" : { "entity_created" : { "new_task" : "New task" } }
        }
      },
      { "data_set" : { "data" : [ "new-task:rid" ], "value" : [ "trigger-task:rid" ] } },
      { "data_set" : { "data" : [ "new-task:task-status" ], "value" : "0" } },
      { "data_set" : { "data" : [ "new-task:task-type" ], "value" : "rebuild_registry" } },
      { "entity_save" : { "data" : [ "new-task" ], "immediate" : "1" } }
    ]
  }
}

{ "rules_flush_cache" : {
    "LABEL" : "Flush cache",
    "PLUGIN" : "rule",
    "OWNER" : "rules",
    "TAGS" : [ "hosting task" ],
    "REQUIRES" : [ "rules" ],
    "USES VARIABLES" : { "trigger_task" : { "label" : "Triggering task", "type" : "node" } },
    "IF" : [
      { "entity_is_of_type" : { "entity" : [ "trigger-task" ], "type" : "node" } },
      { "entity_is_of_bundle" : {
          "entity" : [ "trigger-task" ],
          "type" : "node",
          "bundle" : { "value" : { "task" : "task" } }
        }
      }
    ],
    "DO" : [
      { "entity_create" : {
          "USING" : {
            "type" : "node",
            "param_type" : "task",
            "param_title" : "[trigger-task:title]",
            "param_author" : [ "trigger-task:author" ]
          },
          "PROVIDE" : { "entity_created" : { "new_task" : "New task" } }
        }
      },
      { "data_set" : { "data" : [ "new-task:rid" ], "value" : [ "trigger-task:rid" ] } },
      { "data_set" : { "data" : [ "new-task:task-status" ], "value" : "0" } },
      { "data_set" : { "data" : [ "new-task:task-type" ], "value" : "flush_cache" } },
      { "entity_save" : { "data" : [ "new-task" ], "immediate" : "1" } }
    ]
  }
}
