<?php

/**
 * @file
 * Aegir Rules module.
 */

/**
 * Implements hook_rules_event_info().
 *
 * @ingroup rules
 */
function aegir_rules_rules_event_info() {

  $definitions = aegir_rules_event_definitions();

  $events = array();

  foreach ($definitions as $task_type => $definition) {
    foreach ($definition['tasks'] as $task_name => $task_label) {
      foreach (array('pre', 'post') as $drush_hook) {
        $events[$drush_hook . '_' . 'hosting_' . $task_type . '_' . $task_name] = array(
          'label' => $drush_hook . ': ' . $task_label,
          'variables' => $definition['variables'],
          'group' => 'Hosting ' . $drush_hook . ' ' . $task_type,
        );
      }
    }
  }

  return $events;
}

/**
 * Implements hook_rules_condition_info().
 *
 * @ingroup rules
 */
function aegir_rules_rules_condition_info() {
  return array(
    'aegir_rules_condition_task_status_processing' => array(
      'label' => t('Task is processing'),
      'parameter' => array(
        'task' => array(
          'type' => 'node',
          'label' => t('Task object'),
        ),
      ),
      'group' => 'Hosting',
    ),
    'aegir_rules_condition_task_status_queued' => array(
      'label' => t('Task is queued'),
      'parameter' => array(
        'task' => array(
          'type' => 'node',
          'label' => t('Task object'),
        ),
      ),
      'group' => 'Hosting',
    ),
    'aegir_rules_condition_task_status_success' => array(
      'label' => t('Task completed successfully'),
      'parameter' => array(
        'task' => array(
          'type' => 'node',
          'label' => t('Task object'),
        ),
      ),
      'group' => 'Hosting',
    ),
    'aegir_rules_condition_task_status_failure' => array(
      'label' => t('Task failed to complete'),
      'parameter' => array(
        'task' => array(
          'type' => 'node',
          'label' => t('Task object'),
        ),
      ),
      'group' => 'Hosting',
    ),
  );
}

function aegir_rules_rules_action_info() {
  $items = array();

  $items['aegir_rules_action_add_task'] = array(
    'label' => t('Add task on a site'),
    'group' => t('Hostmaster'),
    'parameter' => array(
      'site_node' => array(
        'type' => 'node',
        'label' => t('Site node'),
      ),
    ),
  );

  $items['aegir_rules_action_add_platform'] = array(
    'label' => t('Add a platform'),
    'group' => t('Hostmaster'),
    'parameter' => array(
      'author' => array(
        'type' => 'user',
        'label' => t('User, who is set as author'),
       ),
       'title' => array(
          'type' => 'string',
          'label' => t('Name'),
          'description' => t('The a unique descriptive name for your platform.'),
        ),
        'publish_path' => array(
          'type' => 'string',
          'label' => t('Publish path'),
          'description' => t('The absolute path on the filesystem where the sites will be hosted.'),
        ),
        'makefile' => array(
          'type' => 'string',
          'label' => t('Makefile'),
          'description' => t('The makefile that will be used to create the platform.'),
        ),
    ),
    'provides' => array(
      'node_added' => array(
        'type' => 'node',
        'label' => t('New platform'),
        'save' => TRUE,
      ),
    ),
  );

  return $items;
}

/**
 * Returns rules events definitions for default Aegir hosting tasks.
 */
function aegir_rules_event_definitions() {
  $definitions = array(
    'default' => array(
      'tasks' => array(
        'execute' => t('A task is executed'),
      ),
      'variables' => array(
        'task' => array(
          'type' => 'node',
          'label' => t('Node: Task'),
        ),
        'reference' => array(
          'type' => 'node',
          'label' => t('Node: Reference'),
        ),
      ),
    ),
    'site' => array(
      'tasks' => array(
        'clone' => t('Site cloned'),
        'migrate' => t('Site migrated'),
        'backup' => t('Site backed up'),
        'restore' => t('Site restored'),
        'verify' => t('Site verified'),
        'disable' => t('Site disabled'),
        'enable' => t('Site enabled'),
        'delete' => t('Site deleted'),
        'login-reset' => t('Site login reset'),
        'backup-delete' => t('Site backup deleleted'),
        'install' => t('Site installed'),
        'import' => t('Site imported'),
      ),
      'variables' => array(
        'task' => array(
          'type' => 'node',
          'label' => t('Node: Task'),
        ),
        'site' => array(
          'type' => 'node',
          'label' => t('Node: Site'),
        ),
      ),
    ),
    'platform' => array(
      'tasks' => array(
        'migrate' => t('Platform migrated'),
        'verify' => t('Platform verification'),
        'delete' => t('Platform deleted'),
        'lock' => t('Platform locked'),
        'unlock' => t('Platform unlocked'),
      ),
      'variables' => array(
        'task' => array(
          'type' => 'node',
          'label' => t('Node: Task'),
        ),
        'platform' => array(
          'type' => 'node',
          'label' => t('Node: Platform'),
        ),
      ),
    ),
    'server' => array(
      'tasks' => array(
        'verify' => t('Server verification'),
      ),
      'variables' => array(
        'task' => array(
          'type' => 'node',
          'label' => t('Node: Task'),
        ),
        'server' => array(
          'type' => 'node',
          'label' => t('Node: Server'),
        ),
      ),
    ),
  );

  return $definitions;
}

/**
 * Add a new task to the queue.
 */
function aegir_rules_action_add_task() {
}

/**
 * Action "Add a platform".
 */
function aegir_rules_action_add_platform($author, $title, $publish_path, $makefile, $settings) {
  if (!$settings['node_access'] || node_access('create', 'platform', $author)) {
    $node = array(
      'type' => 'platform',
      'name' => $author->name,
      'uid' => $author->uid,
      'title' => $title,
      'publish_path' => $publish_path,
      'makefile' => $makefile,
      'web_server' => variable_get('hosting_default_web_server', 2),
    );
    return entity_create('node', $node);
  }
}

/**
 * Rules condition function to check if task is processing.
 */
function aegir_rules_condition_task_status_processing($task) {
  return ($task->task_status == HOSTING_TASK_PROCESSING);
}

/**
 * Rules condition function to check if task is queued.
 */
function aegir_rules_condition_task_status_queued($task) {
  return ($task->task_status == HOSTING_TASK_QUEUED);
}

/**
 * Rules condition function to check if task was successful.
 */
function aegir_rules_condition_task_status_success($task) {
  return ($task->task_status == HOSTING_TASK_SUCCESS);
}

/**
 * Rules condition function to check if task failed.
 */
function aegir_rules_condition_task_status_failure($task) {
  return ($task->task_status == HOSTING_TASK_ERROR);
}
